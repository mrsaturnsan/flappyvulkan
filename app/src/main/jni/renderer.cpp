#include <renderer.h>
#include <logger.h>
#include <engine.h>
#include <vector>
#include <functional>
#include <cassert>
#include <array>
#include <limits>
#include <cstring>
#include <algorithm>
#include <glm/gtc/matrix_transform.hpp>

#define TO_STRING(x) #x

#define vk_call(func, ...)\
if (func(__VA_ARGS__) != VK_SUCCESS)\
{\
    fv::logger::log_message_e("%s %s", TO_STRING(func), "failed");\
    std::terminate();\
}

namespace
{
    const std::vector<const char*> INSTANCE_EXTENSIONS {
#ifdef VALIDATION_LAYERS
        VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
#endif
        "VK_KHR_surface",
        "VK_KHR_android_surface"
    };

    const std::vector<const char*> DEVICE_EXTENSIONS {
        "VK_KHR_swapchain"
    };

#ifdef VALIDATION_LAYERS
    const std::vector<const char*> INSTANCE_LAYERS {
        "VK_LAYER_GOOGLE_threading",
        "VK_LAYER_LUNARG_parameter_validation",
        "VK_LAYER_LUNARG_object_tracker",
        "VK_LAYER_LUNARG_core_validation",
        "VK_LAYER_GOOGLE_unique_objects"
    };

    
    VKAPI_ATTR VkBool32 VKAPI_CALL debug_report_callback(VkDebugReportFlagsEXT msgFlags,
                                                         VkDebugReportObjectTypeEXT,
                                                         uint64_t, size_t,
                                                         int32_t msgCode, const char* pLayerPrefix,
                                                         const char* pMsg, void*)
    {
        if (msgFlags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
        {
            fv::logger::log_message_e("ERROR: [%s] Code %i : %s", pLayerPrefix, msgCode, pMsg);
        }
        else if (msgFlags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
        {
            fv::logger::log_message_w("WARNING: [%s] Code %i : %s", pLayerPrefix, msgCode, pMsg);
        }
        else if (msgFlags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
        {
            fv::logger::log_message_w("PERFORMANCE WARNING: [%s] Code %i : %s", pLayerPrefix, msgCode, pMsg);
        }
        else if (msgFlags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
        {
            fv::logger::log_message_i("INFO: [%s] Code %i : %s", pLayerPrefix, msgCode, pMsg);
        }
        else if (msgFlags & VK_DEBUG_REPORT_DEBUG_BIT_EXT)
        {
            fv::logger::log_message_v("DEBUG: [%s] Code %i : %s", pLayerPrefix, msgCode, pMsg);
        }

        return VK_FALSE;
    }

#endif
}

namespace fv
{
    renderer::renderer()
    {
        _create_instance();
#ifdef VALIDATION_LAYERS
        _create_debug_callback();
#endif
        _create_physical_device();
        _create_logical_device();
        _create_command_pool();
        _create_texture();
        _create_square_mesh();
        _create_surface();
        _create_swap_chain();
        _create_render_pass();
        _create_descriptor_set_layout();
        _create_graphics_pipeline();
        _create_framebuffers(m_renderInf.m_renderPass);
        _create_cmd_buffers();
        _create_uniform_buffers();
        _create_descriptor_pool();
        _create_descriptor_sets();
        _create_sync_objects();
        _create_projection_matrix();
    }

    renderer::~renderer()
    {
        if (m_surface != VK_NULL_HANDLE)
        {
            destroy_window();
        }

        vkDestroyDescriptorPool(m_logicalDevice, m_desInf.m_descriptorPool, nullptr);

        vkDestroyDescriptorSetLayout(m_logicalDevice, m_desInf.m_descriptorSetLayout, nullptr);

        for (size_t i = 0; i < m_swapChain.m_swapchainLength; ++i)
        {
            vkDestroyBuffer(m_logicalDevice, m_uniformBuffers.m_uniformBuffer[i], nullptr);
            vkFreeMemory(m_logicalDevice, m_uniformBuffers.m_uniformBuffersMem[i], nullptr);
        }

        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
        {
            vkDestroySemaphore(m_logicalDevice, m_renderInf.m_renderFinishedSemaphores[i], nullptr);
            vkDestroySemaphore(m_logicalDevice, m_renderInf.m_imageAvailableSemaphores[i], nullptr);
            vkDestroyFence(m_logicalDevice, m_renderInf.m_inFlightFences[i], nullptr);
        }

        vkDestroyCommandPool(m_logicalDevice, m_renderInf.m_cmdPool, nullptr);

        m_sprite.reset();

        m_squareMesh.reset();

        vkDestroyDevice(m_logicalDevice, nullptr);

#ifdef VALIDATION_LAYERS
        PFN_vkDestroyDebugReportCallbackEXT vkDestroyDebugReportCallbackEXT = 
            reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_instance, "vkDestroyDebugReportCallbackEXT"));
        assert(vkDestroyDebugReportCallbackEXT);
        vkDestroyDebugReportCallbackEXT(m_instance, m_debugReportCallback, nullptr);
#endif

        vkDestroyInstance(m_instance, nullptr);
    }

    void renderer::window_created()
    {
        _create_surface();
        
        _construct_core();
    }

    void renderer::destroy_window()
    {
        vkDeviceWaitIdle(m_logicalDevice);

        _destroy_swap_chain();

        vkDestroySurfaceKHR(m_instance, m_surface, nullptr);

        m_surface = VK_NULL_HANDLE;
    }

    void renderer::begin_frame()
    {
        if (m_surface == VK_NULL_HANDLE) return;
        
        for (uint32_t bufferIndex = 0; bufferIndex < m_swapChain.m_swapchainLength; ++bufferIndex)
        {
            VkCommandBufferBeginInfo cmdBufferBeginInfo
            {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                .pNext = nullptr,
                .flags = 0,
                .pInheritanceInfo = nullptr
            };

            vk_call(vkBeginCommandBuffer, m_renderInf.m_cmdBuffer[bufferIndex], &cmdBufferBeginInfo);

            // Now we start a renderpass. Any draw command has to be recorded in a
            // renderpass
            VkClearValue clearVals
            {
                .color.float32[0] = 1.0f,
                .color.float32[1] = 1.0f,
                .color.float32[2] = 1.0f,
                .color.float32[3] = 1.0f
            };

            VkRenderPassBeginInfo renderPassBeginInfo
            {
                .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
                .pNext = nullptr,
                .renderPass = m_renderInf.m_renderPass,
                .framebuffer = m_swapChain.m_framebuffers[bufferIndex],
                .renderArea = 
                {
                    .offset =
                    {
                        .x = 0,
                        .y = 0
                    },
                    .extent = m_swapChain.m_displaySize
                },
                .clearValueCount = 1,
                .pClearValues = &clearVals
            };

            vkCmdBeginRenderPass(m_renderInf.m_cmdBuffer[bufferIndex], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
            
            // Bind what is necessary to the command buffer
            vkCmdBindPipeline(m_renderInf.m_cmdBuffer[bufferIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, m_gfxPipeline.m_pipeline);

            m_squareMesh->draw(m_renderInf.m_cmdBuffer[bufferIndex], [&]{vkCmdBindDescriptorSets(m_renderInf.m_cmdBuffer[bufferIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, m_gfxPipeline.m_layout, 0, 1, &m_desInf.m_descriptorSets[bufferIndex], 0, nullptr);});
        }
    }

    void renderer::end_frame()
    {
        if (m_surface == VK_NULL_HANDLE) return;

        _end_frame();

        // Get the framebuffer index we should draw in
        uint32_t nextIndex;
        VkResult result = vkAcquireNextImageKHR(m_logicalDevice, m_swapChain.m_swapChain, std::numeric_limits<uint64_t>::max(),
                                                m_renderInf.m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &nextIndex);

        if (result == VK_ERROR_OUT_OF_DATE_KHR)
        {
            _recreate_swap_chain();
            return;
        }
        else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
        {
            logger::log_message_e("%s", "Failed to acquire swap chain image");
            std::terminate();
        }

        VkPipelineStageFlags waitStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

        VkSubmitInfo submit_info
        {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pNext = nullptr,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = &m_renderInf.m_imageAvailableSemaphores[m_currentFrame],
            .pWaitDstStageMask = &waitStages,
            .commandBufferCount = 1,
            .pCommandBuffers = &m_renderInf.m_cmdBuffer[nextIndex],
            .signalSemaphoreCount = 1,
            .pSignalSemaphores = &m_renderInf.m_renderFinishedSemaphores[m_currentFrame]
        };

        vk_call(vkResetFences, m_logicalDevice, 1, &m_renderInf.m_inFlightFences[m_currentFrame]);
        vk_call(vkQueueSubmit, m_queue, 1, &submit_info, m_renderInf.m_inFlightFences[m_currentFrame]);

        VkPresentInfoKHR presentInfo
        {
            .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
            .pNext = nullptr,
            .swapchainCount = 1,
            .pSwapchains = &m_swapChain.m_swapChain,
            .pImageIndices = &nextIndex,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = &m_renderInf.m_renderFinishedSemaphores[m_currentFrame],
            .pResults = &result,
        };

        result = vkQueuePresentKHR(m_queue, &presentInfo);

        vk_call(vkWaitForFences, m_logicalDevice, 1, &m_renderInf.m_inFlightFences[m_currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());
        
        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
        {
            _recreate_swap_chain();
        }
        else if (result != VK_SUCCESS)
        {
            logger::log_message_e("%s", "Failed to present swap chain image");
            std::terminate();
        }
        else
        {
            _reset_cmd_buffers(m_currentFrame);
        }

        _update_matrix(m_currentFrame);

        m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
    }

    VkDevice renderer::get_device() const
    {
        return m_logicalDevice;
    }

    void renderer::create_buffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
                                 VkBuffer& buffer, VkDeviceMemory& bufferMemory)
    {
        VkBufferCreateInfo bufferInfo
        {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .size = size,
            .usage = usage,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE
        };

        vk_call(vkCreateBuffer, m_logicalDevice, &bufferInfo, nullptr, &buffer);

        VkMemoryRequirements memRequirements;
        vkGetBufferMemoryRequirements(m_logicalDevice, buffer, &memRequirements);
        
        uint32_t memTypeIndex;
        _map_memory_type_to_index(memRequirements.memoryTypeBits, properties, memTypeIndex);

        VkMemoryAllocateInfo allocInfo
        {
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .allocationSize = memRequirements.size,
            .memoryTypeIndex = memTypeIndex
        };

        vk_call(vkAllocateMemory, m_logicalDevice, &allocInfo, nullptr, &bufferMemory);

        vkBindBufferMemory(m_logicalDevice, buffer, bufferMemory, 0);
    }

    void renderer::copy_buffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size)
    {
        VkCommandBufferAllocateInfo allocInfo
        {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandPool = m_renderInf.m_cmdPool,
            .commandBufferCount = 1
        };

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(m_logicalDevice, &allocInfo, &commandBuffer);

        VkCommandBufferBeginInfo beginInfo
        {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
        };

        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        VkBufferCopy copyRegion
        {
            .size = size
        };

        vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

        vkEndCommandBuffer(commandBuffer);

        VkSubmitInfo submitInfo
        {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .commandBufferCount = 1,
            .pCommandBuffers = &commandBuffer
        };

        vkQueueSubmit(m_queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(m_queue);

        vkFreeCommandBuffers(m_logicalDevice, m_renderInf.m_cmdPool, 1, &commandBuffer);
    }

    VkCommandBuffer renderer::begin_single_time_commands()
    {
        VkCommandBufferAllocateInfo allocInfo
        {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandPool = m_renderInf.m_cmdPool,
            .commandBufferCount = 1
        };

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(m_logicalDevice, &allocInfo, &commandBuffer);

        VkCommandBufferBeginInfo beginInfo
        {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
        };

        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        return commandBuffer;
    }

    void renderer::end_single_time_commands(VkCommandBuffer commandBuffer)
    {
        vkEndCommandBuffer(commandBuffer);

        VkSubmitInfo submitInfo
        {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .commandBufferCount = 1,
            .pCommandBuffers = &commandBuffer
        };

        vkQueueSubmit(m_queue, 1, &submitInfo, VK_NULL_HANDLE);
        vkQueueWaitIdle(m_queue);

        vkFreeCommandBuffers(m_logicalDevice, m_renderInf.m_cmdPool, 1, &commandBuffer);
    }

    void renderer::create_image(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage,
                                VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory)
    {
        VkImageCreateInfo imageInfo
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
            .imageType = VK_IMAGE_TYPE_2D,
            .extent.width = width,
            .extent.height = height,
            .extent.depth = 1,
            .mipLevels = 1,
            .arrayLayers = 1,
            .format = format,
            .tiling = tiling,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .usage = usage,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE
        };

        vk_call(vkCreateImage, m_logicalDevice, &imageInfo, nullptr, &image);

        VkMemoryRequirements memRequirements;
        vkGetImageMemoryRequirements(m_logicalDevice, image, &memRequirements);

        uint32_t memTypeIndex;
        _map_memory_type_to_index(memRequirements.memoryTypeBits, properties, memTypeIndex);

        VkMemoryAllocateInfo allocInfo
        {
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .allocationSize = memRequirements.size,
            .memoryTypeIndex = memTypeIndex
        };

        vk_call(vkAllocateMemory, m_logicalDevice, &allocInfo, nullptr, &imageMemory)

        vkBindImageMemory(m_logicalDevice, image, imageMemory, 0);
    }

    void renderer::set_model_matrix(const glm::mat4& model)
    {
        m_ubo.m_model = model;
    }

    void renderer::_create_instance()
    {
        VkApplicationInfo appInfo
        {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pNext = nullptr,
            .apiVersion = VK_MAKE_VERSION(1, 0, 0),
            .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
            .engineVersion = VK_MAKE_VERSION(1, 0, 0),
            .pApplicationName = "Flappy Vulkan",
            .pEngineName = "FlappyVulkanEngine"
        };

        // Create the Vulkan instance
        VkInstanceCreateInfo instanceCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pNext = nullptr,
            .pApplicationInfo = &appInfo,
            .enabledExtensionCount = static_cast<uint32_t>(INSTANCE_EXTENSIONS.size()),
            .ppEnabledExtensionNames = INSTANCE_EXTENSIONS.data(),
#ifdef VALIDATION_LAYERS
            .enabledLayerCount = static_cast<uint32_t>(INSTANCE_LAYERS.size()),
            .ppEnabledLayerNames = INSTANCE_LAYERS.data()
#else
            .enabledLayerCount = 0,
            .ppEnabledLayerNames = nullptr
#endif
        };

        vk_call(vkCreateInstance, &instanceCreateInfo, nullptr, &m_instance);
    }

#ifdef VALIDATION_LAYERS
        void renderer::_create_debug_callback()
        {
            PFN_vkCreateDebugReportCallbackEXT vkCreateDebugReportCallbackEXT = 
                reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_instance, "vkCreateDebugReportCallbackEXT"));

            assert(vkCreateDebugReportCallbackEXT);

            VkDebugReportCallbackCreateInfoEXT debugReportCallbackCreateInfo
            {
                .sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,
                .pNext = nullptr,
                .flags = VK_DEBUG_REPORT_ERROR_BIT_EXT |
                         VK_DEBUG_REPORT_WARNING_BIT_EXT |
                         VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
                         VK_DEBUG_REPORT_INFORMATION_BIT_EXT,
                .pfnCallback = debug_report_callback,
                .pUserData = nullptr
            };

            vk_call(vkCreateDebugReportCallbackEXT, m_instance, &debugReportCallbackCreateInfo, nullptr, &m_debugReportCallback);
        }
#endif

    void renderer::_create_surface()
    {
        // Create then Vulkan surface
        VkAndroidSurfaceCreateInfoKHR createInfo
        {
            .sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR,
            .pNext = nullptr,
            .flags = 0,
            .window = engine::get_singleton().get_app()->window
        };

        vk_call(vkCreateAndroidSurfaceKHR, m_instance, &createInfo, nullptr, &m_surface);

        _log_surface_info();
    }

    void renderer::_create_physical_device()
    {
        // Find a GPU to use:
        uint32_t gpuCount = 0;
        vk_call(vkEnumeratePhysicalDevices, m_instance, &gpuCount, nullptr);

        // Get the first device
        std::vector<VkPhysicalDevice> tmpGpus(gpuCount);
        vk_call(vkEnumeratePhysicalDevices, m_instance, &gpuCount, tmpGpus.data());
        m_physicalDevice = tmpGpus.front();

        _log_gpu_info();

        // Find a GFX queue family
        uint32_t queueFamilyCount;
        vkGetPhysicalDeviceQueueFamilyProperties(m_physicalDevice, &queueFamilyCount, nullptr);
        assert(queueFamilyCount);

        std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(m_physicalDevice, &queueFamilyCount, queueFamilyProperties.data());

        while (m_queueFamilyIndex < queueFamilyCount)
        {
            if (queueFamilyProperties[m_queueFamilyIndex].queueFlags & VK_QUEUE_GRAPHICS_BIT)
            {
                break;
            }

            ++m_queueFamilyIndex;
        }

        assert(m_queueFamilyIndex < queueFamilyCount);
    }

    void renderer::_log_gpu_info()
    {
        VkPhysicalDeviceProperties gpuProperties;
        vkGetPhysicalDeviceProperties(m_physicalDevice, &gpuProperties);

        logger::log_message_i("Vulkan Physical Device Name: %s", gpuProperties.deviceName);
        logger::log_message_i("Vulkan Physical Device Info: apiVersion = %x, driverVersion = %x",
                              gpuProperties.apiVersion, gpuProperties.driverVersion);
        logger::log_message_i("API Version Supported: %d.%d.%d",
                              VK_VERSION_MAJOR(gpuProperties.apiVersion),
                              VK_VERSION_MINOR(gpuProperties.apiVersion),
                              VK_VERSION_PATCH(gpuProperties.apiVersion));
    }

    void renderer::_log_surface_info()
    {
        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_physicalDevice, m_surface, &surfaceCapabilities);

        logger::log_message_i("%s", "Vulkan Surface Capabilities:\n");
        logger::log_message_i("    image count: %u - %u\n", surfaceCapabilities.minImageCount,
            surfaceCapabilities.maxImageCount);
        logger::log_message_i("    array layers: %u\n", surfaceCapabilities.maxImageArrayLayers);
        logger::log_message_i("    image size (now): %dx%d\n", surfaceCapabilities.currentExtent.width,
            surfaceCapabilities.currentExtent.height);
        logger::log_message_i("    image size (extent): %dx%d - %dx%d\n",
            surfaceCapabilities.minImageExtent.width,
            surfaceCapabilities.minImageExtent.height,
            surfaceCapabilities.maxImageExtent.width,
            surfaceCapabilities.maxImageExtent.height);
        logger::log_message_i("    usage: %x\n", surfaceCapabilities.supportedUsageFlags);
        logger::log_message_i("    current transform: %u\n", surfaceCapabilities.currentTransform);
        logger::log_message_i("    allowed transforms: %x\n", surfaceCapabilities.supportedTransforms);
        logger::log_message_i("    composite alpha flags: %u\n", surfaceCapabilities.currentTransform);
    }

    void renderer::_create_logical_device()
    {
        // Create a logical device from GPU we picked
        std::array<float, 1> priorities { 1.0f };

        VkDeviceQueueCreateInfo queueCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .queueCount = 1,
            .queueFamilyIndex = m_queueFamilyIndex,
            .pQueuePriorities = priorities.data()
        };

        VkDeviceCreateInfo deviceCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .pNext = nullptr,
            .queueCreateInfoCount = 1,
            .pQueueCreateInfos = &queueCreateInfo,
            .enabledLayerCount = 0,
            .ppEnabledLayerNames = nullptr,
            .enabledExtensionCount = static_cast<uint32_t>(DEVICE_EXTENSIONS.size()),
            .ppEnabledExtensionNames = DEVICE_EXTENSIONS.data(),
            .pEnabledFeatures = nullptr
        };

        vk_call(vkCreateDevice, m_physicalDevice, &deviceCreateInfo, nullptr, &m_logicalDevice);
        vkGetDeviceQueue(m_logicalDevice, m_queueFamilyIndex, 0, &m_queue);
    }

    VkExtent2D renderer::_choose_swap_chain_extent(const VkSurfaceCapabilitiesKHR& capabilities)
    {
        if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
        {
            return capabilities.currentExtent;
        }
        else
        {
            android_app* app = engine::get_singleton().get_app();

            int32_t width = ANativeWindow_getWidth(app->window);
            int32_t height = ANativeWindow_getHeight(app->window);

            VkExtent2D actualExtent
            {
                static_cast<uint32_t>(width),
                static_cast<uint32_t>(height)
            };

            actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
            actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));

            return actualExtent;
        }
    }

    void renderer::_create_swap_chain()
    {
        VkSurfaceCapabilitiesKHR surfaceCapabilities;
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_physicalDevice, m_surface, &surfaceCapabilities);

        // Query the list of supported surface format and choose one we like
        uint32_t formatCount = 0;
        vkGetPhysicalDeviceSurfaceFormatsKHR(m_physicalDevice, m_surface, &formatCount, nullptr);

        std::vector<VkSurfaceFormatKHR> formats(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(m_physicalDevice, m_surface, &formatCount, formats.data());

        uint32_t chosenFormat = 0;
        while (chosenFormat < formatCount)
        {
            if (formats[chosenFormat].format == VK_FORMAT_R8G8B8A8_UNORM)
            {
                break;
            }

            ++chosenFormat;
        }

        assert(chosenFormat < formatCount);

        m_swapChain.m_displaySize = _choose_swap_chain_extent(surfaceCapabilities);
        m_swapChain.m_displayFormat = formats[chosenFormat].format;

        VkSwapchainCreateInfoKHR swapchainCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
            .pNext = nullptr,
            .surface = m_surface,
            .minImageCount = surfaceCapabilities.minImageCount,
            .imageFormat = formats[chosenFormat].format,
            .imageColorSpace = formats[chosenFormat].colorSpace,
            .imageExtent = surfaceCapabilities.currentExtent,
            .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
            .preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR,
            .imageArrayLayers = 1,
            .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .queueFamilyIndexCount = 1,
            .pQueueFamilyIndices = &m_queueFamilyIndex,
            .presentMode = VK_PRESENT_MODE_FIFO_KHR,
            .oldSwapchain = VK_NULL_HANDLE,
            .clipped = VK_FALSE,
            .compositeAlpha = VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR
        };

        vk_call(vkCreateSwapchainKHR, m_logicalDevice, &swapchainCreateInfo, nullptr, &m_swapChain.m_swapChain);
        vk_call(vkGetSwapchainImagesKHR, m_logicalDevice, m_swapChain.m_swapChain, &m_swapChain.m_swapchainLength, nullptr);
    }

    void renderer::_destroy_swap_chain()
    {
        for (auto fb : m_swapChain.m_framebuffers)
        {
            vkDestroyFramebuffer(m_logicalDevice, fb, nullptr);
        }

        vkFreeCommandBuffers(m_logicalDevice, m_renderInf.m_cmdPool, m_swapChain.m_swapchainLength, m_renderInf.m_cmdBuffer.data());

        _destroy_graphics_pipeline();
        vkDestroyRenderPass(m_logicalDevice, m_renderInf.m_renderPass, nullptr);

        for (auto imageView : m_swapChain.m_displayViews)
        {
            vkDestroyImageView(m_logicalDevice, imageView, nullptr);
        }

        vkDestroySwapchainKHR(m_logicalDevice, m_swapChain.m_swapChain, nullptr);
    }

    void renderer::_create_render_pass()
    {
        VkAttachmentDescription attachmentDescriptions
        {
            .format = m_swapChain.m_displayFormat,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
        };

        VkAttachmentReference colourReference
        {
            .attachment = 0,
            .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        };

        VkSubpassDescription subpassDescription
        {
            .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
            .flags = 0,
            .inputAttachmentCount = 0,
            .pInputAttachments = nullptr,
            .colorAttachmentCount = 1,
            .pColorAttachments = &colourReference,
            .pResolveAttachments = nullptr,
            .pDepthStencilAttachment = nullptr,
            .preserveAttachmentCount = 0,
            .pPreserveAttachments = nullptr,
        };

        VkRenderPassCreateInfo renderPassCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            .pNext = nullptr,
            .attachmentCount = 1,
            .pAttachments = &attachmentDescriptions,
            .subpassCount = 1,
            .pSubpasses = &subpassDescription,
            .dependencyCount = 0,
            .pDependencies = nullptr,
        };

        vk_call(vkCreateRenderPass, m_logicalDevice, &renderPassCreateInfo, nullptr, &m_renderInf.m_renderPass);
    }

    void renderer::_create_framebuffers(VkRenderPass& renderPass, VkImageView depthView)
    {
        // query display attachment to swapchain
        uint32_t swapChainImageCount = 0;
        vk_call(vkGetSwapchainImagesKHR, m_logicalDevice, m_swapChain.m_swapChain, &swapChainImageCount, nullptr);
        m_swapChain.m_displayImages.resize(swapChainImageCount);

        vk_call(vkGetSwapchainImagesKHR, m_logicalDevice, m_swapChain.m_swapChain,
                &swapChainImageCount, m_swapChain.m_displayImages.data());

        // create image view for each swapchain image
        m_swapChain.m_displayViews.resize(swapChainImageCount);

        for (uint32_t i = 0; i < swapChainImageCount; ++i)
        {
            VkImageViewCreateInfo viewCreateInfo
            {
                .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
                .pNext = nullptr,
                .image = m_swapChain.m_displayImages[i],
                .viewType = VK_IMAGE_VIEW_TYPE_2D,
                .format = m_swapChain.m_displayFormat,
                .components =
                {
                    .r = VK_COMPONENT_SWIZZLE_R,
                    .g = VK_COMPONENT_SWIZZLE_G,
                    .b = VK_COMPONENT_SWIZZLE_B,
                    .a = VK_COMPONENT_SWIZZLE_A,
                },
                .subresourceRange =
                {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .baseMipLevel = 0,
                    .levelCount = 1,
                    .baseArrayLayer = 0,
                    .layerCount = 1,
                },
                .flags = 0
            };

            vk_call(vkCreateImageView, m_logicalDevice, &viewCreateInfo, nullptr, &m_swapChain.m_displayViews[i]);
        }

        // create a framebuffer from each swapchain image
        m_swapChain.m_framebuffers.resize(m_swapChain.m_swapchainLength);

        for (uint32_t i = 0; i < m_swapChain.m_swapchainLength; ++i)
        {
            std::array<VkImageView, 2> attachments { m_swapChain.m_displayViews[i], depthView };

            VkFramebufferCreateInfo fbCreateInfo
            {
                .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
                .pNext = nullptr,
                .renderPass = renderPass,
                .layers = 1,
                .attachmentCount = 1,  // 2 if using depth
                .pAttachments = attachments.data(),
                .width = m_swapChain.m_displaySize.width,
                .height = m_swapChain.m_displaySize.height,
            };

            fbCreateInfo.attachmentCount = (depthView == VK_NULL_HANDLE ? 1 : 2);

            vk_call(vkCreateFramebuffer, m_logicalDevice, &fbCreateInfo, nullptr, &m_swapChain.m_framebuffers[i]);
        }
    }

    void renderer::_create_cmd_buffers()
    {
        m_renderInf.m_cmdBuffer.resize(m_swapChain.m_swapchainLength);

        for (uint32_t bufferIndex = 0; bufferIndex < m_swapChain.m_swapchainLength; ++bufferIndex)
        {
            // We start by creating and declare the "beginning" our command buffer
            VkCommandBufferAllocateInfo cmdBufferCreateInfo
            {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .pNext = nullptr,
                .commandPool = m_renderInf.m_cmdPool,
                .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1
            };

            vk_call(vkAllocateCommandBuffers, m_logicalDevice, &cmdBufferCreateInfo, &m_renderInf.m_cmdBuffer[bufferIndex]);
        }
    }

    void renderer::_create_texture()
    {
        m_sprite = std::make_unique<texture>("sprite.png");
    }

    void renderer::_create_square_mesh()
    {
        const std::vector<float> vertices
        {
            -0.5f, -0.5f,
            0.5f, -0.5f,
            0.5f, 0.5f,
            -0.5f, 0.5f
        };

        const std::vector<uint16_t> indices
        {
            2, 3, 0, 2, 0, 1
        };

        m_squareMesh = std::make_unique<mesh>(vertices, indices);
    }

    void renderer::_create_descriptor_set_layout()
    {
        VkDescriptorSetLayoutBinding uboLayoutBinding
        {
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
            .pImmutableSamplers = nullptr
        };

        VkDescriptorSetLayoutBinding samplerLayoutBinding
        {
            .binding = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
            .pImmutableSamplers = nullptr

        };

        std::array<VkDescriptorSetLayoutBinding, 2> bindings { uboLayoutBinding, samplerLayoutBinding };

        VkDescriptorSetLayoutCreateInfo layoutInfo
        {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            .bindingCount = static_cast<uint32_t>(bindings.size()),
            .pBindings = bindings.data()
        };

        vk_call(vkCreateDescriptorSetLayout, m_logicalDevice, &layoutInfo, nullptr, &m_desInf.m_descriptorSetLayout);
    }

    void renderer::_create_descriptor_sets()
    {
        std::vector<VkDescriptorSetLayout> layouts(m_swapChain.m_swapchainLength, m_desInf.m_descriptorSetLayout);

        VkDescriptorSetAllocateInfo allocInfo
        {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            .descriptorPool = m_desInf.m_descriptorPool,
            .descriptorSetCount = m_swapChain.m_swapchainLength,
            .pSetLayouts = layouts.data()
        };

        m_desInf.m_descriptorSets.resize(m_swapChain.m_swapchainLength);

        vk_call(vkAllocateDescriptorSets, m_logicalDevice, &allocInfo, m_desInf.m_descriptorSets.data());

        for (size_t i = 0; i < m_swapChain.m_swapchainLength; ++i)
        {
            VkDescriptorBufferInfo bufferInfo
            {
                .buffer = m_uniformBuffers.m_uniformBuffer[i],
                .offset = 0,
                .range = sizeof(UniformBufferObject)
            };

            VkDescriptorImageInfo imageInfo
            {
                .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                .imageView = m_sprite->get_image_view(),
                .sampler = m_sprite->get_sampler()
            };

            std::array<VkWriteDescriptorSet, 2> descriptorWrites
            {
                VkWriteDescriptorSet
                {
                    .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                    .dstSet = m_desInf.m_descriptorSets[i],
                    .dstBinding = 0,
                    .dstArrayElement = 0,
                    .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .descriptorCount = 1,
                    .pBufferInfo = &bufferInfo
                },
                VkWriteDescriptorSet
                {
                    .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                    .dstSet = m_desInf.m_descriptorSets[i],
                    .dstBinding = 1,
                    .dstArrayElement = 0,
                    .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    .descriptorCount = 1,
                    .pImageInfo  = &imageInfo
                }
            };

            vkUpdateDescriptorSets(m_logicalDevice, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
        }
    }

    void renderer::_create_graphics_pipeline()
    {
        // Create pipeline layout (empty)
        VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
            .pNext = nullptr,
            .setLayoutCount = 1,
            .pSetLayouts = &m_desInf.m_descriptorSetLayout,
            .pushConstantRangeCount = 0,
            .pPushConstantRanges = nullptr
        };

        vk_call(vkCreatePipelineLayout, m_logicalDevice, &pipelineLayoutCreateInfo, nullptr, &m_gfxPipeline.m_layout);

        VkShaderModule vertexShader, fragmentShader;
        _load_shader_from_file("shaders/sprite.vert.spv", vertexShader, ShaderType::VERTEX_SHADER);
        _load_shader_from_file("shaders/sprite.frag.spv", fragmentShader, ShaderType::FRAGMENT_SHADER);

        // Specify vertex and fragment shader stages
        std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages
        {
            VkPipelineShaderStageCreateInfo
            {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                .pNext = nullptr,
                .stage = VK_SHADER_STAGE_VERTEX_BIT,
                .module = vertexShader,
                .pSpecializationInfo = nullptr,
                .flags = 0,
                .pName = "main"
            },
            VkPipelineShaderStageCreateInfo
            {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                .pNext = nullptr,
                .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
                .module = fragmentShader,
                .pSpecializationInfo = nullptr,
                .flags = 0,
                .pName = "main"
            }
        };

        VkViewport viewports
        {
            .minDepth = 0.0f,
            .maxDepth = 1.0f,
            .x = 0.0f,
            .y = 0.0f,
            .width = static_cast<float>(m_swapChain.m_displaySize.width),
            .height = static_cast<float>(m_swapChain.m_displaySize.height)
        };

        VkRect2D scissor
        {
            .extent = m_swapChain.m_displaySize,
            .offset =
            {
                .x = 0,
                .y = 0
            }
        };

        // Specify viewport info
        VkPipelineViewportStateCreateInfo viewportInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
            .pNext = nullptr,
            .viewportCount = 1,
            .pViewports = &viewports,
            .scissorCount = 1,
            .pScissors = &scissor,
        };

        // Specify multisample info
        VkSampleMask sampleMask = ~0u;
        VkPipelineMultisampleStateCreateInfo multisampleInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            .pNext = nullptr,
            .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
            .sampleShadingEnable = VK_FALSE,
            .minSampleShading = 0,
            .pSampleMask = &sampleMask,
            .alphaToCoverageEnable = VK_FALSE,
            .alphaToOneEnable = VK_FALSE
        };

        // Specify color blend state
        VkPipelineColorBlendAttachmentState attachmentStates
        {
            .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
            .blendEnable = VK_TRUE,
            .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
            .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
            .colorBlendOp = VK_BLEND_OP_ADD,
            .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
            .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
            .alphaBlendOp = VK_BLEND_OP_ADD
        };

        VkPipelineColorBlendStateCreateInfo colorBlendInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            .pNext = nullptr,
            .logicOpEnable = VK_FALSE,
            .logicOp = VK_LOGIC_OP_COPY,
            .attachmentCount = 1,
            .pAttachments = &attachmentStates,
            .flags = 0
        };

        // Specify rasterizer info
        VkPipelineRasterizationStateCreateInfo rasterInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            .pNext = nullptr,
            .depthClampEnable = VK_FALSE,
            .rasterizerDiscardEnable = VK_FALSE,
            .polygonMode = VK_POLYGON_MODE_FILL,
            .cullMode = VK_CULL_MODE_BACK_BIT,
            .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
            .depthBiasEnable = VK_FALSE,
            .lineWidth = 1
        };

        // Specify input assembler state
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            .pNext = nullptr,
            .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            .primitiveRestartEnable = VK_FALSE
        };

        auto bindingDescription = mesh::get_binding_description();
        auto attributeDescriptions = mesh::get_attribute_descriptions();

        VkPipelineVertexInputStateCreateInfo vertexInputInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            .pNext = nullptr,
            .vertexBindingDescriptionCount = 1,
            .pVertexBindingDescriptions = &bindingDescription,
            .vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size()),
            .pVertexAttributeDescriptions = attributeDescriptions.data()
        };

        // Create the pipeline cache
        VkPipelineCacheCreateInfo pipelineCacheInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO,
            .pNext = nullptr,
            .initialDataSize = 0,
            .pInitialData = nullptr,
            .flags = 0,  // reserved, must be 0
        };

        vk_call(vkCreatePipelineCache, m_logicalDevice, &pipelineCacheInfo, nullptr, &m_gfxPipeline.m_cache);

        // Create the pipeline
        VkGraphicsPipelineCreateInfo pipelineCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .stageCount = static_cast<uint32_t>(shaderStages.size()),
            .pStages = shaderStages.data(),
            .pVertexInputState = &vertexInputInfo,
            .pInputAssemblyState = &inputAssemblyInfo,
            .pTessellationState = nullptr,
            .pViewportState = &viewportInfo,
            .pRasterizationState = &rasterInfo,
            .pMultisampleState = &multisampleInfo,
            .pDepthStencilState = nullptr,
            .pColorBlendState = &colorBlendInfo,
            .pDynamicState = nullptr,
            .layout = m_gfxPipeline.m_layout,
            .renderPass = m_renderInf.m_renderPass,
            .subpass = 0,
            .basePipelineHandle = VK_NULL_HANDLE,
            .basePipelineIndex = 0
        };

        vk_call(vkCreateGraphicsPipelines, m_logicalDevice, m_gfxPipeline.m_cache, 1, &pipelineCreateInfo, nullptr, &m_gfxPipeline.m_pipeline);

        // We don't need the shaders anymore, we can release their memory
        vkDestroyShaderModule(m_logicalDevice, vertexShader, nullptr);
        vkDestroyShaderModule(m_logicalDevice, fragmentShader, nullptr);
    }

    void renderer::_destroy_graphics_pipeline()
    {
        vkDestroyPipeline(m_logicalDevice, m_gfxPipeline.m_pipeline, nullptr);
        vkDestroyPipelineCache(m_logicalDevice, m_gfxPipeline.m_cache, nullptr);
        vkDestroyPipelineLayout(m_logicalDevice, m_gfxPipeline.m_layout, nullptr);
    }

    void renderer::_create_command_pool()
    {
        VkCommandPoolCreateInfo cmdPoolCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .pNext = nullptr,
            .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
            .queueFamilyIndex = 0
        };

        vk_call(vkCreateCommandPool, m_logicalDevice, &cmdPoolCreateInfo, nullptr, &m_renderInf.m_cmdPool);
    }

    void renderer::_create_descriptor_pool()
    {
        std::array<VkDescriptorPoolSize, 2> poolSizes
        {
            VkDescriptorPoolSize
            {
                .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = m_swapChain.m_swapchainLength

            },
            VkDescriptorPoolSize
            {
                .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = m_swapChain.m_swapchainLength
            }
        };

        VkDescriptorPoolCreateInfo poolInfo
        {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
            .poolSizeCount = static_cast<uint32_t>(poolSizes.size()),
            .pPoolSizes = poolSizes.data(),
            .maxSets = m_swapChain.m_swapchainLength
        };

        vk_call(vkCreateDescriptorPool, m_logicalDevice, &poolInfo, nullptr, &m_desInf.m_descriptorPool);
    }

    void renderer::_create_sync_objects()
    {
        m_renderInf.m_imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
        m_renderInf.m_renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
        m_renderInf.m_inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

        VkSemaphoreCreateInfo semaphoreInfo
        {
            .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
        };

        VkFenceCreateInfo fenceInfo
        {
            .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            .flags = VK_FENCE_CREATE_SIGNALED_BIT
        };

        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
        {
            vk_call(vkCreateSemaphore, m_logicalDevice, &semaphoreInfo, nullptr, &m_renderInf.m_imageAvailableSemaphores[i]);
            vk_call(vkCreateSemaphore, m_logicalDevice, &semaphoreInfo, nullptr, &m_renderInf.m_renderFinishedSemaphores[i]);
            vk_call(vkCreateFence, m_logicalDevice, &fenceInfo, nullptr, &m_renderInf.m_inFlightFences[i]);
        }
    }

    void renderer::_create_uniform_buffers()
    {
        VkDeviceSize bufferSize = sizeof(UniformBufferObject);

        m_uniformBuffers.m_uniformBuffer.resize(m_swapChain.m_swapchainLength);
        m_uniformBuffers.m_uniformBuffersMem.resize(m_swapChain.m_swapchainLength);

        for (size_t i = 0; i < m_swapChain.m_swapchainLength; ++i)
        {
            create_buffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                          VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                          m_uniformBuffers.m_uniformBuffer[i], m_uniformBuffers.m_uniformBuffersMem[i]);
        }
    }

    void renderer::_recreate_swap_chain()
    {
        vkDeviceWaitIdle(m_logicalDevice);

        _destroy_swap_chain();

        _construct_core();
    }

    void renderer::_construct_core()
    {
        _create_swap_chain();
        _create_render_pass();
        _create_graphics_pipeline();
        _create_framebuffers(m_renderInf.m_renderPass);
        _create_cmd_buffers();
    }

    void renderer::_load_shader_from_file(const char* filePath, VkShaderModule& shaderOut, ShaderType type)
    {
        AAsset* file = AAssetManager_open(engine::get_singleton().get_app()->activity->assetManager, filePath, AASSET_MODE_BUFFER);
        size_t fileLength = AAsset_getLength(file);

        std::vector<char> fileContent(fileLength);

        AAsset_read(file, fileContent.data(), fileLength);
        AAsset_close(file);

        VkShaderModuleCreateInfo shaderModuleCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
            .pNext = nullptr,
            .codeSize = fileLength,
            .pCode = reinterpret_cast<const uint32_t*>(fileContent.data()),
            .flags = 0
        };

        vk_call(vkCreateShaderModule, m_logicalDevice, &shaderModuleCreateInfo, nullptr, &shaderOut);
    }

    bool renderer::_map_memory_type_to_index(uint32_t typeBits, VkFlags requirements_mask, uint32_t& typeIndex)
    {
        VkPhysicalDeviceMemoryProperties memoryProperties;
        vkGetPhysicalDeviceMemoryProperties(m_physicalDevice, &memoryProperties);

        // Search memtypes to find first index with those properties
        for (uint32_t i = 0; i < 32; ++i)
        {
            if ((typeBits & 1) == 1)
            {
                // Type is available, does it match user properties?
                if ((memoryProperties.memoryTypes[i].propertyFlags & requirements_mask) == requirements_mask)
                {
                    typeIndex = i;
                    return true;
                }
            }

            typeBits >>= 1;
        }

        return false;
    }

    void renderer::_end_frame()
    {
        for (uint32_t bufferIndex = 0; bufferIndex < m_swapChain.m_swapchainLength; ++bufferIndex)
        {
            vkCmdEndRenderPass(m_renderInf.m_cmdBuffer[bufferIndex]);
            vk_call(vkEndCommandBuffer, m_renderInf.m_cmdBuffer[bufferIndex]);
        }
    }

    void renderer::_reset_cmd_buffers(size_t current_frame)
    {
        vkResetCommandBuffer(m_renderInf.m_cmdBuffer[current_frame], VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
    }

    void renderer::_update_matrix(size_t current)
    {
        void* data;
        vkMapMemory(m_logicalDevice, m_uniformBuffers.m_uniformBuffersMem[current], 0, sizeof(UniformBufferObject), 0, &data);
        std::memcpy(data, &m_ubo, sizeof(UniformBufferObject));
        vkUnmapMemory(m_logicalDevice, m_uniformBuffers.m_uniformBuffersMem[current]);
    }

    void renderer::_create_projection_matrix()
    {
        constexpr float WORLD_SIZE = 5.0f;
        constexpr float NEAR = -1.0f;
        constexpr float FAR = 1.0f;

        float ratio = static_cast<float>(m_swapChain.m_displaySize.width) / static_cast<float>(m_swapChain.m_displaySize.height);
        m_ubo.m_projView = glm::ortho(-WORLD_SIZE * ratio, WORLD_SIZE * ratio, -WORLD_SIZE, WORLD_SIZE, NEAR, FAR);
        m_ubo.m_projView[1][1] *= -1;
    }
}
