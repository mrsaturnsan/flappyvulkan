#include <engine.h>
#include <logger.h>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>

namespace
{
    void handle_cmd(android_app* app, int32_t cmd)
    {
        fv::engine* engine = reinterpret_cast<fv::engine*>(app->userData);
        fv::renderer& renderer = engine->get_renderer();

        switch (cmd)
        {
            case APP_CMD_INIT_WINDOW:
                renderer.window_created();
                break;
            case APP_CMD_TERM_WINDOW:
                renderer.destroy_window();
                break;
            default:
                fv::logger::log_message_i("%s: %d", "Event not handled", cmd);
        }
    }

    int32_t handle_input(android_app* app, AInputEvent* event)
    {
        if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION)
        {
            if (AKeyEvent_getAction(event) == AMOTION_EVENT_ACTION_DOWN)
            {
                reinterpret_cast<fv::engine*>(app->userData)->m_jump = true;
            }

            return 1;
        }

        return 0;
    }
}

namespace fv
{
    engine::engine(android_app* app) : m_app{app}, m_renderer{}
    {
        m_app->userData = this;
        m_app->onAppCmd = handle_cmd;
        m_app->onInputEvent = handle_input;
    }

    engine::~engine()
    {
        m_app->userData = nullptr;
        m_app->onAppCmd = nullptr;
        m_app->onInputEvent = nullptr;
    }

    android_app* engine::get_app() const
    {
        return m_app;
    }

    renderer& engine::get_renderer()
    {
        return m_renderer;
    }

    void engine::execute_game()
    {
        int events;
        android_poll_source* source;

        do
        {
            if (ALooper_pollAll(1, nullptr, &events, reinterpret_cast<void**>(&source)) >= 0)
            {
                if (source)
                {
                    source->process(m_app, source);
                }  
            }

            _update();
        }
        while (m_app->destroyRequested == 0);
    }

    void engine::_update()
    {
        m_renderer.begin_frame();

        _game();

        m_renderer.end_frame();
    }

    void engine::_game()
    {
        if (m_jump)
        {
            m_jump = false;
            m_speed = J_SPEED;
        }

        m_speed -= GRAVITY * DT;
        m_pos.y += m_speed * DT;
        
        if (m_pos.y < GROUND)
        {
            m_speed = 0.0f;
            m_pos.y = GROUND;
        }
        else if (m_pos.y > CEIL)
        {
            m_pos.y = CEIL;
        }

        static auto startTime = std::chrono::high_resolution_clock::now();
        auto currentTime = std::chrono::high_resolution_clock::now();
        float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();
        m_renderer.set_model_matrix(glm::translate(glm::mat4(1.0f), m_pos) * glm::rotate(glm::mat4(1.0f), time * glm::radians(ROTATION_SPEED), glm::vec3(0.0f, 0.0f, 1.0f)));
    }
}
