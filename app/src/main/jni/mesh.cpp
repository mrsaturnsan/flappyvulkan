#include <mesh.h>
#include <renderer.h>
#include <engine.h>
#include <cstring>

namespace fv
{
    VkVertexInputBindingDescription mesh::get_binding_description()
    {
        VkVertexInputBindingDescription bindingDescription
        {
            .binding = 0,
            .stride = sizeof(Vertex),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
        };

        return bindingDescription;
    }
        
    std::array<VkVertexInputAttributeDescription, 1> mesh::get_attribute_descriptions()
    {
        std::array<VkVertexInputAttributeDescription, 1> attributeDescriptions
        {
            VkVertexInputAttributeDescription
            {
                .binding = 0,
                .location = 0,
                .format = VK_FORMAT_R32G32_SFLOAT,
                .offset = offsetof(Vertex, m_pos)
            }
        };

        return attributeDescriptions;
    }

    mesh::mesh(const std::vector<float>& vertexBuf,
               const std::vector<uint16_t>& indexBuf) : m_numIndices{static_cast<uint32_t>(indexBuf.size())}
    {
        _create_vertex_buffer(vertexBuf);
        _create_index_buffer(indexBuf);
    }

    mesh::~mesh()
    {
        VkDevice device = engine::get_singleton().get_renderer().get_device();
        vkDestroyBuffer(device, m_vertexBuf.m_buffer, nullptr);
        vkFreeMemory(device, m_vertexBuf.m_bufferMem, nullptr);
        vkDestroyBuffer(device, m_indexBuf.m_buffer, nullptr);
        vkFreeMemory(device, m_indexBuf.m_bufferMem, nullptr);
    }

    void mesh::draw(const VkCommandBuffer& cmdBuffer) const
    {
        VkDeviceSize offset = 0;
        vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &m_vertexBuf.m_buffer, &offset);
        vkCmdBindIndexBuffer(cmdBuffer, m_indexBuf.m_buffer, 0, VK_INDEX_TYPE_UINT16);
        vkCmdDrawIndexed(cmdBuffer, m_numIndices, 1, 0, 0, 0);
    }

    void mesh::_create_vertex_buffer(const std::vector<float>& vertexBuf)
    {
        VkDeviceSize bufferSize = sizeof(float) * vertexBuf.size();
        renderer& rndr = engine::get_singleton().get_renderer();
        VkDevice device = rndr.get_device();

        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;
        rndr.create_buffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                           stagingBuffer, stagingBufferMemory);

        void* data;
        vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
        std::memcpy(data, vertexBuf.data(), static_cast<size_t>(bufferSize));
        vkUnmapMemory(device, stagingBufferMemory);

        rndr.create_buffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                           m_vertexBuf.m_buffer, m_vertexBuf.m_bufferMem);

        rndr.copy_buffer(stagingBuffer, m_vertexBuf.m_buffer, bufferSize);

        vkDestroyBuffer(device, stagingBuffer, nullptr);
        vkFreeMemory(device, stagingBufferMemory, nullptr);
    }

    void mesh::_create_index_buffer(const std::vector<uint16_t>& indexBuf)
    {
        VkDeviceSize bufferSize = sizeof(uint16_t) * indexBuf.size();
        renderer& rndr = engine::get_singleton().get_renderer();
        VkDevice device = rndr.get_device();

        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;
        rndr.create_buffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                           stagingBuffer, stagingBufferMemory);

        void* data;
        vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
        std::memcpy(data, indexBuf.data(), static_cast<size_t>(bufferSize));
        vkUnmapMemory(device, stagingBufferMemory);

        rndr.create_buffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                           VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_indexBuf.m_buffer, m_indexBuf.m_bufferMem);

        rndr.copy_buffer(stagingBuffer, m_indexBuf.m_buffer, bufferSize);

        vkDestroyBuffer(device, stagingBuffer, nullptr);
        vkFreeMemory(device, stagingBufferMemory, nullptr);
    }
}
