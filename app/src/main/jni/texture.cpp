#include <texture.h>
#include <engine.h>
#include <logger.h>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include <stb_image.h>

#include <cstring>
#include "headers/texture.h"

namespace fv
{
    texture::texture(const std::string& file_path)
    {
        _create_image(file_path);
        _create_image_view();
        _create_sampler();
    }

    texture::~texture()
    {
        const renderer& rndr = engine::get_singleton().get_renderer();
        VkDevice device = rndr.get_device();

        vkDestroySampler(device, m_imageSampler, nullptr);
        vkDestroyImageView(device, m_imageView, nullptr);
        vkDestroyImage(device, m_image, nullptr);
        vkFreeMemory(device, m_imageMem, nullptr);
    }

    VkImageView texture::get_image_view()
    {
        return m_imageView;
    }

    VkSampler texture::get_sampler()
    {
        return m_imageSampler;
    }

    void texture::_create_image(const std::string& file_path)
    {
        android_app* app = engine::get_singleton().get_app();

        AAsset* file = AAssetManager_open(app->activity->assetManager, file_path.c_str(), AASSET_MODE_BUFFER);
        size_t fileLength = AAsset_getLength(file);
        std::vector<stbi_uc> fileContent(fileLength);
        AAsset_read(file, fileContent.data(), fileLength);
        AAsset_close(file);

        int texWidth, texHeight, texChannels;
        stbi_uc* pixels = stbi_load_from_memory(fileContent.data(), fileLength, &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
        VkDeviceSize imageSize = texWidth * texHeight * 4;

        if (!pixels)
        {
            logger::log_message_e("%s", "Failed to load image");
            std::terminate();
        }

        renderer& rndr = engine::get_singleton().get_renderer();
        VkDevice device = rndr.get_device();

        VkBuffer stagingBuffer;
        VkDeviceMemory stagingBufferMemory;
        rndr.create_buffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                           stagingBuffer, stagingBufferMemory);

        void* data;
        vkMapMemory(device, stagingBufferMemory, 0, imageSize, 0, &data);
        std::memcpy(data, pixels, static_cast<size_t>(imageSize));
        vkUnmapMemory(device, stagingBufferMemory);

        stbi_image_free(pixels);

        rndr.create_image(texWidth, texHeight, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL,
                          VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                          VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_image, m_imageMem);

        _transition_image_layout(m_image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED,
                                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
        _copy_buffer_to_image(stagingBuffer, m_image, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
        _transition_image_layout(m_image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                                 VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

        vkDestroyBuffer(device, stagingBuffer, nullptr);
        vkFreeMemory(device, stagingBufferMemory, nullptr);
    }

    void texture::_create_image_view()
    {
        renderer& rndr = engine::get_singleton().get_renderer();
        VkDevice device = rndr.get_device();

        VkImageViewCreateInfo viewInfo
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = m_image,
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = VK_FORMAT_R8G8B8A8_UNORM,
            .subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .subresourceRange.baseMipLevel = 0,
            .subresourceRange.levelCount = 1,
            .subresourceRange.baseArrayLayer = 0,
            .subresourceRange.layerCount = 1
        };

        if (vkCreateImageView(device, &viewInfo, nullptr, &m_imageView) != VK_SUCCESS)
        {
            logger::log_message_e("%s", "Failed to create image view");
            std::terminate();
        }
    }

    void texture::_create_sampler()
    {
        renderer& rndr = engine::get_singleton().get_renderer();
        VkDevice device = rndr.get_device();

        VkSamplerCreateInfo samplerInfo
        {
            .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
            .magFilter = VK_FILTER_LINEAR,
            .minFilter = VK_FILTER_LINEAR,
            .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
            .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
            .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
            .anisotropyEnable = VK_FALSE,
            .maxAnisotropy = 1,
            .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
            .unnormalizedCoordinates = VK_FALSE,
            .compareEnable = VK_FALSE,
            .compareOp = VK_COMPARE_OP_ALWAYS,
            .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR
        };

        if (vkCreateSampler(device, &samplerInfo, nullptr, &m_imageSampler) != VK_SUCCESS)
        {
            logger::log_message_e("%s", "Failed to create texutre sampler");
            std::terminate();
        }
    }

    void texture::_transition_image_layout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
    {
        renderer& rndr = engine::get_singleton().get_renderer();
        VkCommandBuffer commandBuffer = rndr.begin_single_time_commands();

        VkImageMemoryBarrier barrier
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .oldLayout = oldLayout,
            .newLayout = newLayout,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .image = image,
            .subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .subresourceRange.baseMipLevel = 0,
            .subresourceRange.levelCount = 1,
            .subresourceRange.baseArrayLayer = 0,
            .subresourceRange.layerCount = 1
        };

        VkPipelineStageFlags sourceStage;
        VkPipelineStageFlags destinationStage;

        if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
        {
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        }
        else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
        {
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

            sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        }
        else
        {
            logger::log_message_e("%s", "Unsupported layout transition");
            std::terminate();
        }

        vkCmdPipelineBarrier(commandBuffer, sourceStage, destinationStage,
                             0, 0, nullptr, 0, nullptr, 1, &barrier);

        rndr.end_single_time_commands(commandBuffer);
    }

    void texture::_copy_buffer_to_image(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
    {
        renderer& rndr = engine::get_singleton().get_renderer();
        VkCommandBuffer commandBuffer = rndr.begin_single_time_commands();

        VkBufferImageCopy region
        {
            .bufferOffset = 0,
            .bufferRowLength = 0,
            .bufferImageHeight = 0,
            .imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .imageSubresource.mipLevel = 0,
            .imageSubresource.baseArrayLayer = 0,
            .imageSubresource.layerCount = 1,
            .imageOffset = {0, 0, 0},
            .imageExtent =
            {
                width,
                height,
                1
            }
        };

        vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

        rndr.end_single_time_commands(commandBuffer);
    }
}
