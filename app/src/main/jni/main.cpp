#include <android_native_app_glue.h>
#include <engine.h>

void android_main(android_app* app)
{
    auto handleCmd = [] (android_app* app, int32_t cmd)
    {
        if (cmd == APP_CMD_INIT_WINDOW)
        {
            *reinterpret_cast<bool*>(app->userData) = true;
        }
    };

    bool windowCreated = false;

    // Set the callback to process system events
    app->userData = &windowCreated;
    app->onAppCmd = handleCmd;

    // Used to poll the events in the main loop
    int events;
    android_poll_source* source;

    // Wait until window is created
    do
    {
        if (ALooper_pollAll(0, nullptr, &events, reinterpret_cast<void**>(&source)) >= 0)
        {
            if (source)
            {
                source->process(app, source);
            }
        }
    }
    while (!windowCreated && app->destroyRequested == 0);

    if (app->destroyRequested == 0)
    {
        app->userData = nullptr;
        app->onAppCmd = nullptr;
        
        fv::engine engine{app};
        engine.execute_game();
    }
}
