#ifndef ENGINE_HPP
#define ENGINE_HPP

#include <android_native_app_glue.h>

#include <utility.h>
#include <renderer.h>

namespace fv
{
    class engine : public singleton<engine>
    {
    private:
        static constexpr float GRAVITY = 18.5f;
        static constexpr float GROUND = -4.5f;
        static constexpr float CEIL = 7.0f;
        static constexpr float J_SPEED = 11.0f;
        static constexpr float DT = 0.01667f;
        static constexpr float ROTATION_SPEED = 90.0f;

        android_app* m_app;
        renderer m_renderer;
    public:
        bool m_jump = false;
        float m_speed = 0.0f;
        glm::vec3 m_pos{0.0f, 0.0f, 0.0f};
    public:
        engine(android_app* app);
        ~engine();
        android_app* get_app() const;
        renderer& get_renderer();
        void execute_game();
    private:
        void _update();
        void _game();
    };
}

#endif
