#ifndef LOGGER_H
#define LOGGER_H

#include <android/log.h>
#include <utility>

namespace fv::logger
{
    template <typename... Args>
    void log_message_v(Args&&... args)
    {
        __android_log_print(ANDROID_LOG_VERBOSE, APP_NAME, std::forward<Args>(args)...);
    }

    template <typename... Args>
    void log_message_d(Args&&... args)
    {
        __android_log_print(ANDROID_LOG_DEBUG, APP_NAME, std::forward<Args>(args)...);
    }

    template <typename... Args>
    void log_message_i(Args&&... args)
    {
        __android_log_print(ANDROID_LOG_INFO, APP_NAME, std::forward<Args>(args)...);
    }

    template <typename... Args>
    void log_message_w(Args&&... args)
    {
        __android_log_print(ANDROID_LOG_WARN, APP_NAME, std::forward<Args>(args)...);
    }

    template <typename... Args>
    void log_message_e(Args&&... args)
    {
        __android_log_print(ANDROID_LOG_ERROR, APP_NAME, std::forward<Args>(args)...);
    }

    template <typename... Args>
    void log_message_f(Args&&... args)
    {
        __android_log_print(ANDROID_LOG_FATAL, APP_NAME, std::forward<Args>(args)...);
    }
}

#endif
