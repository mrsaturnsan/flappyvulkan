#ifndef RENDERER_H
#define RENDERER_H

#include <vulkan/vulkan.h>
#include <vector>
#include <memory>
#include <mesh.h>
#include <texture.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

namespace fv
{
    class renderer
    {
    private:
        static constexpr size_t MAX_FRAMES_IN_FLIGHT = 2;
    private:
        struct VulkanSwapchainInfo
        {
            VkSwapchainKHR m_swapChain;
            uint32_t m_swapchainLength = 0;

            VkExtent2D m_displaySize;
            VkFormat m_displayFormat;

            std::vector<VkImage> m_displayImages;
            std::vector<VkImageView> m_displayViews;
            std::vector<VkFramebuffer> m_framebuffers;
        };

        struct VulkanGfxPipelineInfo
        {
            VkPipelineLayout m_layout;
            VkPipelineCache m_cache;
            VkPipeline m_pipeline;
        };

        struct VulkanRenderInf
        {
            VkRenderPass m_renderPass;
            VkCommandPool m_cmdPool;
            std::vector<VkCommandBuffer> m_cmdBuffer;
            std::vector<VkSemaphore> m_imageAvailableSemaphores;
            std::vector<VkSemaphore> m_renderFinishedSemaphores;
            std::vector<VkFence> m_inFlightFences;
        };

        struct UniformBuffer
        {
            std::vector<VkBuffer> m_uniformBuffer;
            std::vector<VkDeviceMemory> m_uniformBuffersMem;
        };

        struct UniformBufferObject
        {
            alignas(16) glm::mat4 m_model = glm::mat4(1.0f);
            alignas(16) glm::mat4 m_projView = glm::mat4(1.0f);
        };

        struct DescriptorInf
        {
            VkDescriptorSetLayout m_descriptorSetLayout;
            VkDescriptorPool m_descriptorPool;
            std::vector<VkDescriptorSet> m_descriptorSets;
        };

        enum class ShaderType
        {
            VERTEX_SHADER,
            FRAGMENT_SHADER
        };
    private:
        VkInstance m_instance;
#ifdef VALIDATION_LAYERS
        VkDebugReportCallbackEXT m_debugReportCallback;
#endif
        VkSurfaceKHR m_surface = VK_NULL_HANDLE;
        VkQueue m_queue;
        VkPhysicalDevice m_physicalDevice;
        uint32_t m_queueFamilyIndex = 0;
        VkDevice m_logicalDevice;
        size_t m_currentFrame = 0;
        VulkanSwapchainInfo m_swapChain;
        VulkanRenderInf m_renderInf;
        std::unique_ptr<mesh> m_squareMesh;
        std::unique_ptr<texture> m_sprite;
        VulkanGfxPipelineInfo m_gfxPipeline;
        UniformBuffer m_uniformBuffers;
        DescriptorInf m_desInf;
        UniformBufferObject m_ubo;
    public:
        renderer();
        ~renderer();
        void window_created();
        void destroy_window();
        void begin_frame();
        void end_frame();
        VkDevice get_device() const;
        void create_buffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties,
                            VkBuffer& buffer, VkDeviceMemory& bufferMemory);
        void copy_buffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
        VkCommandBuffer begin_single_time_commands();
        void end_single_time_commands(VkCommandBuffer commandBuffer);
        void create_image(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usage,
                          VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory);
        void set_model_matrix(const glm::mat4& model);
    private:
        void _create_instance();
#ifdef VALIDATION_LAYERS
        void _create_debug_callback();
#endif
        void _create_surface();
        void _create_physical_device();
        void _log_gpu_info();
        void _log_surface_info();
        void _create_logical_device();
        VkExtent2D _choose_swap_chain_extent(const VkSurfaceCapabilitiesKHR& capabilities);
        void _create_swap_chain();
        void _destroy_swap_chain();
        void _create_render_pass();
        void _create_framebuffers(VkRenderPass& renderPass, VkImageView depthView = VK_NULL_HANDLE);
        void _create_cmd_buffers();
        void _create_texture();
        void _create_square_mesh();
        void _create_descriptor_set_layout();
        void _create_descriptor_sets();
        void _create_graphics_pipeline();
        void _destroy_graphics_pipeline();
        void _create_command_pool();
        void _create_descriptor_pool();
        void _create_sync_objects();
        void _create_uniform_buffers();

        void _recreate_swap_chain();
        void _construct_core();

        void _load_shader_from_file(const char* filePath, VkShaderModule& shaderOut, ShaderType type);
        bool _map_memory_type_to_index(uint32_t typeBits, VkFlags requirements_mask, uint32_t& typeIndex);
        void _end_frame();
        void _reset_cmd_buffers(size_t current_frame);
        void _create_projection_matrix();
        void _update_matrix(size_t current);
    };
}

#endif
