#ifndef MESH_H
#define MESH_H

#include <vulkan/vulkan.h>
#include <vector>
#include <array>

namespace fv
{
    class mesh
    {
    private:
        struct Buffer
        {
            VkBuffer m_buffer;
            VkDeviceMemory m_bufferMem;
        };

        struct Vertex
        {
            std::array<float, 2> m_pos;
        };
        
    private:
        Buffer m_vertexBuf;
        Buffer m_indexBuf;
        uint32_t m_numIndices;
    public:
        mesh(const std::vector<float>& vertexBuf,
             const std::vector<uint16_t>& indexBuf);

        ~mesh();

        void draw(const VkCommandBuffer& cmdBuffer) const;
        
        template <typename T>
        void draw(const VkCommandBuffer& cmdBuffer, const T& hak) const
        {
            VkDeviceSize offset = 0;
            vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &m_vertexBuf.m_buffer, &offset);
            vkCmdBindIndexBuffer(cmdBuffer, m_indexBuf.m_buffer, 0, VK_INDEX_TYPE_UINT16);
            hak();
            vkCmdDrawIndexed(cmdBuffer, m_numIndices, 1, 0, 0, 0);
        }

        static VkVertexInputBindingDescription get_binding_description();

        static std::array<VkVertexInputAttributeDescription, 1> get_attribute_descriptions();
    private:
        void _create_vertex_buffer(const std::vector<float>& vertexBuf);
        void _create_index_buffer(const std::vector<uint16_t>& indexBuf);
    };
}

#endif
