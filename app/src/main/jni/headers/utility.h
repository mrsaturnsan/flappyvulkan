#ifndef UTILITY_H
#define UTILITY_H

#include <exception>

namespace fv
{
    template <typename T>
    class singleton
    {
    private:
        inline static T* m_singleton = nullptr;
    protected:
        singleton()
        {
            if (m_singleton)
            {
                std::terminate();
            }
            else
            {
                m_singleton = static_cast<T*>(this);
            }
        }

        ~singleton()
        {
            m_singleton = nullptr;
        }
    public:
        static T& get_singleton()
        {
            assert(m_singleton);
            return static_cast<T&>(*m_singleton);
        }

        static T* get_singleton_ptr()
        {
            assert(m_singleton);
            return static_cast<T*>(m_singleton);
        }
    };

    class nocopy
    {
    protected:
        nocopy() = default;
    private:
        nocopy(const nocopy&) = delete;
        nocopy(nocopy&&) = delete;
        nocopy& operator=(const nocopy&) = delete;
    };
}

#endif
