#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <vulkan/vulkan.h>

namespace fv
{
    class texture
    {
    private:
        VkImage m_image;
        VkDeviceMemory m_imageMem;
        VkImageView m_imageView;
        VkSampler m_imageSampler;
    public:
        texture(const std::string& file_path);
        ~texture();
        VkImageView get_image_view();
        VkSampler get_sampler();
    private:
        void _create_image(const std::string& file_path);
        void _create_image_view();
        void _create_sampler();

        void _transition_image_layout(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);
        void _copy_buffer_to_image(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
    };
}

#endif
