#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (binding = 0) uniform UniformBufferObject
{
    mat4 model;
    mat4 projectView;
} ubo;

layout (location = 0) in vec2 pos;

layout (location = 0) out vec2 texCoord;

void main()
{
    gl_Position = ubo.projectView * ubo.model * vec4(pos, 0.0, 1.0);
    texCoord = pos + vec2(0.5);
    texCoord.y *= -1.0;
}
